package ru.tsc.anaumova.tm.exception.system;

import ru.tsc.anaumova.tm.exception.AbstractException;

public class IncorrectStatusException extends AbstractException {

    public IncorrectStatusException(final String value) {
        super("Error! Incorrect status. Value `" + value + "` not found...");
    }

}