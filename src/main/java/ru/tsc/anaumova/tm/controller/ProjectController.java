package ru.tsc.anaumova.tm.controller;

import ru.tsc.anaumova.tm.api.controller.IProjectController;
import ru.tsc.anaumova.tm.api.service.IProjectService;
import ru.tsc.anaumova.tm.api.service.IProjectTaskService;
import ru.tsc.anaumova.tm.enumerated.Sort;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.model.Project;
import ru.tsc.anaumova.tm.util.DateUtil;
import ru.tsc.anaumova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(
            final IProjectService projectService,
            final IProjectTaskService projectTaskService
    ) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showProjectList() {
        System.out.println("[PROJECT LIST]");
        System.out.println("[ENTER SORT:]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = projectService.findAll(sort);
        int index = 1;
        for (Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public void clearProjectList() {
        System.out.println("[PROJECTS CLEAR]");
        projectService.clear();
    }

    @Override
    public void createProject() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        System.out.println("[ENTER DATE BEGIN:]");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("[ENTER DATE END:]");
        final Date dateEnd = TerminalUtil.nextDate();
        projectService.create(name, description, dateBegin, dateEnd);
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        projectService.updateByIndex(index, name, description);
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        projectService.updateById(id, name, description);
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        projectTaskService.removeProjectById(project.getId());
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        projectTaskService.removeProjectById(project.getId());
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        showProject(project);
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        showProject(project);
    }

    private void showProject(final Project project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(project.getDateEnd()));
    }

    @Override
    public void changeProjectStatusByIndex() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER STATUS:]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        projectService.changeProjectStatusByIndex(index, status);
    }

    @Override
    public void changeProjectStatusById() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS:]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        projectService.changeProjectStatusById(id, status);
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.changeProjectStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void startProjectById() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        projectService.changeProjectStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void completeProjectByIndex() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        projectService.changeProjectStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public void completeProjectById() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        projectService.changeProjectStatusById(id, Status.COMPLETED);
    }

}